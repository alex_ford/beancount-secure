# Beancount Secure (beancount-secure)

This micro-project builds off of the Beancount double-entry accounting CLI project. <http://furius.ca/beancount/>

Beancount Secure is a repository of Python code which I have written to provide security features missing from the base implementation of Beancount, such as the ability to encrypt the description and/or payee of a transaction.

Maturity: **[ Ideation ]** -> [ Prototype ] -> [ Alpha ] -> [ Beta ] -> [ Generally Usable ]

The above is a rough approximation of the maturity of this project and cooresponds to the typical software development release cycle. Ideation represents the earliest stage of the process; there is no code, no software, just some (probably half baked) ideas.

Features

- [TODO] Hide (encrypt) individual transaction descriptions
- [TODO] Hide (encrypt) individual transaction payees

**Author**: Alex Ford (arf4188@gmail.com)

**License**: MIT License (see [LICENSE](LICENSE) file for more details)

## Dependencies

In addition to this Git repo, you will also need the following other repos:

* None

## Quick Start Usage

TBD

## Common Conventions

In all of my beancount-* repos, I follow the following conventions:

* Beancount data files (with your actual transactions in them) use the .bcdat extension
* Beancount spec files (which are supporting files used by my scripts and plugins) use the .bcspec extension